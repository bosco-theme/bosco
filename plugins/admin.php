<?php

// This file is part of the Bosco Theme for WordPress, 
// based on the Carrington Text Theme (http://carringtontheme.com).
// 
// Copyright (c) 2010, Nathan R. Yergler. http://yergler.net
// Copyright (c) 2008-2009 Crowd Favorite, Ltd. All rights reserved.
// http://crowdfavorite.com
//
// Released under the GPL license
// http://www.opensource.org/licenses/gpl-license.php
//
// **********************************************************************
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
// **********************************************************************

if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) { die(); }

global $bosco_options;
$bosco_options = array(
	'bosco_site_title' => 'Site Title',
	'bosco_blog_title' => 'Blog Title',
);

function bosco_blog_settings_form() {

	$html = '
	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th scope="row">Bosco</td>
				<td>
					<fieldset>
						<p>';

	foreach (array_keys($GLOBALS['bosco_options']) as $option_name) {

		$html .= '<label for="'.$option_name.'">'.$bosco_options[$option_name].'</label><input type="text" name="'.$option_name.'" id="'.$option_name.'" value="'.get_option($option_name).'"  /></p>';
	}

	$html .= '</fieldset></td></tr></tbody></table>';
	echo $html;
}
add_action('cfct_settings_form_top', 'bosco_blog_settings_form');

function bosco_update_settings() {
	if (!current_user_can('manage_options')) {
		return;
	}

	foreach (array_keys($GLOBALS['bosco_options']) as $option) {
		if (isset($_POST[$option])) {
			update_option($option, stripslashes($_POST[$option]));
		}
	}
}
add_action('cfct_update_settings', 'bosco_update_settings');

?>
